create database car_rental;
\c car_rental

begin;
create sequence klient_id_seq;

create table klient(
  klient_id integer primary key default nextval('klient_id_seq'),
  nazwisko varchar(300) not null,
  telefon varchar(30) not null,
  email varchar(100),
  adres text
);

create domain silnik_typ as varchar check(value in ('Diesel', 'Na gaz', 'Benzynowy', 'Elektryczny'));
create sequence samochod_id_seq;

create table samochod(
  samochod_id integer primary key default nextval('samochod_id_seq'),
  marka varchar(30),
  poj_silnika numeric,
  typ_silnika silnik_typ,
  liczba_osob smallint
);


create sequence rezerwacja_id_seq; 

create table rezerwacja(
  rezerwacja_id integer primary key default nextval('rezerwacja_id_seq'),
  klient_id integer references klient on delete restrict,
  samochod_id integer references samochod on delete restrict,
  data_od date not null,
  data_do date not null
);


create view rezerwacja_i_samochod as (
  select rezerwacja_id, klient_id, samochod_id, marka, data_od, data_do from
    samochod natural join rezerwacja
);

create rule usuwanie_rezerwacji as on delete to rezerwacja_i_samochod
do instead (delete from rezerwacja r where r.rezerwacja_id = old.rezerwacja_id);


create function intervals_intersect(date, date, date, date) returns boolean
as $x$
begin
  if $1 = $3 and $2 = $4 then
    return true;
  end if;

  if $1 < $3 and $3 < $2 then
    return true;
  end if;
  
  if $3 < $1 and $1 < $4 then
    return true;
  end if;

  return false;
end;
$x$
language plpgsql;


create function dodawanie_rezerwacji() returns trigger as
$x$
begin
  if new.data_od > new.data_do then
    raise exception 'nieprawidłowy przedział';
  end if;


  if exists (select * from rezerwacja r where 
    intervals_intersect(new.data_od, new.data_do, r.data_od, r.data_do) and
    new.samochod_id = r.samochod_id)
  then
    raise exception 'przedziały się przecinają';
  end if;
  
  return new;
end;
$x$
language plpgsql;

create rule dodawanie_rezerwacji_rule as on insert to rezerwacja_i_samochod
do instead insert into rezerwacja(klient_id, samochod_id, data_od, data_do) 
values (new.klient_id, new.samochod_id, new.data_od, new.data_do);

create trigger dodawanie_rezerwacji_trigger before insert on rezerwacja for 
each row execute procedure dodawanie_rezerwacji();



create sequence wypozyczenie_id;

create table wypozyczenie(
  wypozyczenie_id integer primary key default nextval('wypozyczenie_id'),
  klient_id integer references klient on delete restrict,
  samochod_id integer references samochod on delete restrict,
  data_wyp date not null,
  data_zwr date
);


create user klient with password 'optimum9';
create user administrator with password 'optimum10';
create user gosc with password 'optimum11';
create user recepcja with password 'optimum12';

grant select on samochod to gosc;

grant all on samochod_id_seq, klient_id_seq,
  klient, samochod to administrator;

grant select on samochod to klient;
grant select, insert on rezerwacja to klient;
grant all on rezerwacja_i_samochod, rezerwacja_id_seq to klient;

grant select on samochod to recepcja;
grant select, insert, update on wypozyczenie to recepcja;

commit;
