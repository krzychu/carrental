
insert into samochod values 
  (nextval('samochod_id_seq'), 'Ford', 2.4, 'Diesel', 5);
insert into samochod values 
  (nextval('samochod_id_seq'), 'Ferrari', 5.0, 'Benzynowy', 2);
insert into samochod values 
  (nextval('samochod_id_seq'), 'Polonez', 1.6, 'Na gaz', 5);
insert into samochod values 
  (nextval('samochod_id_seq'), 'Hummer', 9000.1, 'Benzynowy', 5);
insert into samochod values 
  (nextval('samochod_id_seq'), 'Melex', 0, 'Elektryczny', 5);
insert into samochod values 
  (nextval('samochod_id_seq'), 'BMW', 5.3, 'Diesel', 7);

