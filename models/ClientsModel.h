/*
 * UsersModel.h
 *
 *  Created on: 13 Apr 2012
 *      Author: krzychu
 */

#ifndef USERSMODEL_H_
#define USERSMODEL_H_

#include <SqlModel.h>

class ClientsModel : public SqlModel{
    Q_OBJECT
  public:
    ClientsModel(QObject * parent, QSqlDatabase db);
    virtual ~ClientsModel();

  public slots:
    void beforeInsertRec(QSqlRecord & rec);
};

#endif /* USERSMODEL_H_ */
