/*
 * CarsModel.cpp
 *
 *  Created on: 11 Apr 2012
 *      Author: krzychu
 */

#include "CarsModel.h"
#include <QtSql>
#include <DBConfig.h>
#include <QDebug>

CarsModel::CarsModel(QObject * parent, QSqlDatabase db)
  : SqlModel(parent, db, "samochod")
{
  engineTypes << "Diesel" << "Na gaz" << "Benzynowy" << "Elektryczny";

  connect(this, SIGNAL(beforeInsert(QSqlRecord &)), this,
      SLOT(beforeInsertRec(QSqlRecord &)));
}

const QStringList & CarsModel::engines(){
  return engineTypes;
}




void CarsModel::beforeInsertRec(QSqlRecord & rec){
  rec.setGenerated("samochod_id", false);
}


void CarsModel::setBookedFilter(bool enabled){
  bookedFilter = enabled;
  setBookedSQL();
}

void CarsModel::setBookedFrom(const QDate & from){
  bookedFrom = from;
  setBookedSQL();
}

void CarsModel::setBookedTo(const QDate & to){
  bookedTo = to;
  setBookedSQL();
}

void CarsModel::setBookedSQL(){
  if(!bookedFilter){
    setFilter("");
    select();
  }
  else{
    QString a = bookedFrom.toString(DBConfig::dateFormat);
    QString b = bookedTo.toString(DBConfig::dateFormat);
    setFilter(QString("not exists (select * from rezerwacja r where\
         (r.samochod_id = samochod.samochod_id) and \
        intervals_intersect(r.data_od, r.data_do, '%1'::date, '%2'::date))").arg(a,b));
    select();
  }
}


CarsModel::~CarsModel(){}

