/*
 * UsersModel.cpp
 *
 *  Created on: 13 Apr 2012
 *      Author: krzychu
 */

#include "ClientsModel.h"
#include <QSqlRecord>

ClientsModel::ClientsModel(QObject * parent, QSqlDatabase db)
  : SqlModel(parent, db, "klient")
{

  connect(this, SIGNAL(beforeInsert(QSqlRecord &)), this,
       SLOT(beforeInsertRec(QSqlRecord &)));
}

void ClientsModel::beforeInsertRec(QSqlRecord & rec){
  rec.setGenerated("klient_id", false);
}

ClientsModel::~ClientsModel() {}

