/*
 * BookingsModel.h
 *
 *  Created on: 14 Apr 2012
 *      Author: krzychu
 */

#ifndef BOOKINGSMODEL_H_
#define BOOKINGSMODEL_H_

#include <SqlModel.h>

class BookingsModel : public SqlModel{
    Q_OBJECT

  public:
    BookingsModel(QObject * parent, QSqlDatabase db, int currentClient);
    virtual ~BookingsModel();

  public slots:
    void beforeInsertRec(QSqlRecord & rec);
};

#endif /* BOOKINGSMODEL_H_ */
