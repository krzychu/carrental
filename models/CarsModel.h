/*
 * CarsModel.h
 *
 *  Created on: 11 Apr 2012
 *      Author: krzychu
 */

#ifndef CARSMODEL_H_
#define CARSMODEL_H_

#include <SqlModel.h>
#include <QStringList>
#include <QDate>

class CarsModel : public SqlModel{
    Q_OBJECT

  public:
    CarsModel(QObject * parent, QSqlDatabase db);
    virtual ~CarsModel();

    const QStringList & engines();

    void setBookedFilter(bool enabled);
    void setBookedFrom(const QDate & from);
    void setBookedTo(const QDate & to);

  public slots:
    void beforeInsertRec(QSqlRecord & rec);

  private:
    QStringList engineTypes;

    bool bookedFilter;
    QDate bookedFrom;
    QDate bookedTo;

    void setBookedSQL();
};

#endif /* CARSMODEL_H_ */
