/*
 * BookingsModel.cpp
 *
 *  Created on: 14 Apr 2012
 *      Author: krzychu
 */

#include "BookingsModel.h"

#include <QSqlRecord>

BookingsModel::BookingsModel(QObject * parent, QSqlDatabase db, int currentClient)
  : SqlModel(parent, db, "rezerwacja_i_samochod")
{

  setFilter(QString(" klient_id = %1").arg(currentClient));

  connect(this, SIGNAL(beforeInsert(QSqlRecord &)), this,
       SLOT(beforeInsertRec(QSqlRecord &)));
}

void BookingsModel::beforeInsertRec(QSqlRecord & rec){
  rec.setGenerated("wypozyczenie_id", false);
  rec.setGenerated("marka", false);
}

BookingsModel::~BookingsModel() {}


