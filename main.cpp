#include <iostream>
#include <QtGui>
#include <CarRentalWindow.h>

int main(int argc, char ** argv){
  QApplication app(argc, argv);
  app.setStyle(new QPlastiqueStyle);
  CarRentalWindow win;
  win.show();

  return app.exec();
}
