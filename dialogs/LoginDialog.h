/*
 * LoginDialog.h
 *
 *  Created on: 11 Apr 2012
 *      Author: krzychu
 */

#ifndef LOGINDIALOG_H_
#define LOGINDIALOG_H_

#include <QDialog>
#include "ui_login.h"

class LoginDialog : public QDialog{
    Q_OBJECT

  public:
    LoginDialog(QWidget * parent = 0);
    virtual ~LoginDialog();

  public slots:
    void roleChanged(int current);
    void loginClicked();

  signals:
    void login(int role, int id);

  private:
    Ui::LoginForm ui;
};

#endif /* LOGINDIALOG_H_ */
