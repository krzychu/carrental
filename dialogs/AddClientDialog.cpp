/*
 * AddClientDialog.cpp
 *
 *  Created on: 13 Apr 2012
 *      Author: krzychu
 */

#include "AddClientDialog.h"
#include <QtSql>
#include <QMessageBox>

AddClientDialog::AddClientDialog(ClientsModel * model_, QWidget * parent)
  : QDialog(parent), model(model_)
{
  ui.setupUi(this);
  connect(ui.cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));
  connect(ui.addButton, SIGNAL(clicked()), this, SLOT(add()));
}


void AddClientDialog::add(){
  QSqlRecord rec = model->record();
  rec.setValue("nazwisko", ui.nameEdit->text());
  rec.setValue("telefon", ui.phoneEdit->text());
  rec.setValue("email", ui.emaliEdit->text());
  rec.setValue("adres", ui.addressEdit->toPlainText());

  bool ok = model->insertRecord(-1, rec);

  if(!ok){
    QMessageBox::critical(this, QString::fromUtf8("Błąd dodawania do bazy"),
        QString::fromUtf8("Dodawanie rekordu nie powiodło się"));
    qDebug() << model->lastError().text();
  }
  else{
    close();
  }
}


void AddClientDialog::cancel(){
  close();
}

AddClientDialog::~AddClientDialog() {}



