/*
 * AddClientDialog.h
 *
 *  Created on: 13 Apr 2012
 *      Author: krzychu
 */

#ifndef ADDCLIENTDIALOG_H_
#define ADDCLIENTDIALOG_H_

#include <QDialog>
#include <ClientsModel.h>
#include "ui_add_client.h"

class AddClientDialog : public QDialog{
    Q_OBJECT
  public:
    AddClientDialog(ClientsModel * model, QWidget * parent = 0);
    virtual ~AddClientDialog();

  private slots:
    void add();
    void cancel();

  private:
    Ui::addClientDialog ui;
    ClientsModel * model;

};

#endif /* ADDCLIENTDIALOG_H_ */
