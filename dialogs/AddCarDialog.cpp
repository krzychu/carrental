/*
 * AddCarDialog.cpp
 *
 *  Created on: 13 Apr 2012
 *      Author: krzychu
 */

#include "AddCarDialog.h"

#include <QDebug>
#include <QMessageBox>
#include <QtSql>

AddCarDialog::AddCarDialog(CarsModel * model_, QWidget * parent)
  : QDialog(parent), model(model_)
{
  ui.setupUi(this);
  connect(ui.cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));
  connect(ui.addButton, SIGNAL(clicked()), this, SLOT(add()));
  ui.engineCombo->addItems(model->engines());
}


void AddCarDialog::add(){
  QSqlRecord rec = model->record();
  rec.setValue("marka", ui.brandEdit->text());
  rec.setValue("poj_silnika", ui.volSpin->value());
  rec.setValue("typ_silnika", ui.engineCombo->currentText());
  rec.setValue("liczba_osob", ui.passengersSpin->value());

  bool ok = model->insertRecord(-1, rec);

  if(!ok){
    QMessageBox::critical(this, QString::fromUtf8("Błąd dodawania do bazy"),
        QString::fromUtf8("Dodawanie rekordu nie powiodło się"));
    qDebug() << model->lastError().text();
  }
  else{
    close();
  }
}


void AddCarDialog::cancel(){
  close();
}

AddCarDialog::~AddCarDialog() {}

