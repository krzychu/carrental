/*
 * LoginDialog.cpp
 *
 *  Created on: 11 Apr 2012
 *      Author: krzychu
 */

#include "LoginDialog.h"
#include <QMessageBox>
#include <DBConfig.h>

LoginDialog::LoginDialog(QWidget * parent)
  : QDialog(parent)
{
  ui.setupUi(this);
  ui.roleCombo->insertItems(0, DBConfig::roleNames);
  this->setModal(true);

  connect(ui.roleCombo, SIGNAL(currentIndexChanged(int)), this,
      SLOT(roleChanged(int)));

  connect(ui.loginButton, SIGNAL(clicked()), this, SLOT(loginClicked()));
}

LoginDialog::~LoginDialog() {

}

void LoginDialog::roleChanged(int current){
  if(current == DBConfig::role::Client){
    ui.idEdit->setEnabled(true);
    ui.idLabel->setEnabled(true);
  }
  else{
    ui.idEdit->setEnabled(false);
    ui.idLabel->setEnabled(false);
  }
}

void LoginDialog::loginClicked(){
  QString id_text = ui.idEdit->displayText();
  if(ui.roleCombo->currentIndex() != DBConfig::role::Client){
    emit login(ui.roleCombo->currentIndex(), 0);
  }
  else{
    bool is_num;
    int id = id_text.toUInt(&is_num);
    if(is_num){
      emit login(DBConfig::role::Client, id);
    }
    else{
      QMessageBox::warning(this, QString::fromUtf8("Błąd logowania"),
          QString::fromUtf8("ID klienta powinno być liczbą"));
    }
  }
}
