/*
 * AddCarDialog.h
 *
 *  Created on: 13 Apr 2012
 *      Author: krzychu
 */

#ifndef ADDCARDIALOG_H_
#define ADDCARDIALOG_H_

#include <QDialog>
#include <CarsModel.h>
#include "ui_add_car.h"


class AddCarDialog : public QDialog{
    Q_OBJECT
  public:
    AddCarDialog(CarsModel * model, QWidget * parent = 0);
    virtual ~AddCarDialog();

  public slots:
    void add();
    void cancel();

  private:
    Ui::addCarDialog ui;
    CarsModel * model;
};

#endif /* ADDCARDIALOG_H_ */
