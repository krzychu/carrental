/*
 * DBConfig.h
 *
 *  Created on: 12 Apr 2012
 *      Author: krzychu
 */

#ifndef DBCONFIG_H_
#define DBCONFIG_H_

#include <QStringList>

namespace DBConfig {
  extern const QString host;
  extern const QString database;
  extern const QString connectionName;

  extern const QStringList users;
  extern const QStringList passwords;

  enum role {Client, Admin, Guest, Reception};

  extern const QStringList roleNames;

  extern const QString dateFormat;
};

#endif /* DBCONFIG_H_ */
