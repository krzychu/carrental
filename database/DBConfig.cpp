/*
 * DBConfig.cpp
 *
 *  Created on: 12 Apr 2012
 *      Author: krzychu
 */

#include "DBConfig.h"

const QString DBConfig::database = "car_rental";
const QString DBConfig::host = "localhost";
const QString DBConfig::connectionName = "CarRentalDBConnection";

const QStringList DBConfig::users({
  "klient",
  "administrator",
  "gosc",
  "recepcja"
});


const QStringList DBConfig::passwords({
  "optimum9",
  "optimum10",
  "optimum11",
  "optimum12"
});

const QStringList DBConfig::roleNames({
  QString::fromUtf8("Kilent"),
  QString::fromUtf8("Administrator"),
  QString::fromUtf8("Gość"),
  QString::fromUtf8("Recepcja")
});

const QString DBConfig::dateFormat = "MM-dd-yyyy";

