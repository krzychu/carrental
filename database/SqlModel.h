/*
 * SqlModel.h
 *
 *  Created on: 13 Apr 2012
 *      Author: krzychu
 */

#ifndef SQLMODEL_H_
#define SQLMODEL_H_

#include <QSqlTableModel>
#include <QSqlDatabase>

class SqlModel : public QSqlTableModel{
    Q_OBJECT
  public:
    SqlModel(QObject * parent, QSqlDatabase db, QString table);
    virtual ~SqlModel();

    Qt::ItemFlags flags(const QModelIndex & index) const;
    bool removeRecords(QModelIndexList items);
};

#endif /* SQLMODEL_H_ */
