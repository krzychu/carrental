/*
 * SqlModel.cpp
 *
 *  Created on: 13 Apr 2012
 *      Author: krzychu
 */

#include "SqlModel.h"
#include <QDebug>


SqlModel::SqlModel(QObject * parent, QSqlDatabase db, QString table_)
  : QSqlTableModel(parent, db)
{
  setTable(table_);
}

Qt::ItemFlags SqlModel::flags(const QModelIndex & index)const {
  return  Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

bool SqlModel::removeRecords(QModelIndexList rows){
  QModelIndexList::iterator itr;
  for(itr = rows.begin(); itr != rows.end(); itr++){
    if(!removeRow(itr->row()))
      return false;
  }
  return true;
}

SqlModel::~SqlModel() {}

