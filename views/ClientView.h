/*
 * ClientView.h
 *
 *  Created on: 14 Apr 2012
 *      Author: krzychu
 */

#ifndef CLIENTVIEW_H_
#define CLIENTVIEW_H_

#include <QWidget>
#include "ui_client.h"
#include <CarsModel.h>
#include <BookingsModel.h>
#include <AddRemoveTableView.h>

class ClientView : public QWidget{
    Q_OBJECT
  public:
    ClientView(int clientId, QWidget * parent = 0);
    virtual ~ClientView();

  public slots:
    void filterChecked(int);
    void filterFromChanged(const QDate &);
    void filterToChanged(const QDate &);

    void bookClicked();

  private:
    CarsModel * carsModel;
    BookingsModel * bookingsModel;
    int clientId;

    Ui::ClientView ui;
    AddRemoveTableView * bookingsView;
};

#endif /* CLIENTVIEW_H_ */
