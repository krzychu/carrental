/*
 * ClientView.cpp
 *
 *  Created on: 14 Apr 2012
 *      Author: krzychu
 */

#include "ClientView.h"
#include <DBConfig.h>
#include <QSqlDatabase>
#include <QDate>
#include <QDebug>
#include <QtSql>
#include <QMessageBox>

ClientView::ClientView(int clientId_, QWidget * parent)
  : QWidget(parent), clientId(clientId_)
{
  ui.setupUi(this);

  // set dates
  QDate today = QDate::currentDate();
  ui.filterFrom->setDate(today);
  ui.filterTo->setDate(today);
  ui.bookFrom->setDate(today);
  ui.bookTo->setDate(today);

  carsModel = new CarsModel(this, QSqlDatabase::database(DBConfig::connectionName));
  ui.carsTable->setModel(carsModel);
  carsModel->select();

  bookingsModel = new BookingsModel(this,
      QSqlDatabase::database(DBConfig::connectionName), clientId);
  bookingsView = new AddRemoveTableView(bookingsModel);
  delete ui.reservationsTab->layout();
  ui.reservationsTab->setLayout(new QGridLayout);
  ui.reservationsTab->layout()->addWidget(bookingsView);

  connect(ui.applyFilter, SIGNAL(stateChanged(int)), this,
      SLOT(filterChecked(int)));
  connect(ui.filterFrom, SIGNAL(dateChanged(const QDate &)),
      this, SLOT(filterFromChanged(const QDate &)));
  connect(ui.filterTo, SIGNAL(dateChanged(const QDate & )),
      this, SLOT(filterToChanged(const QDate &)));
  connect(ui.bookButton, SIGNAL(clicked()), this,
      SLOT(bookClicked()));
}

ClientView::~ClientView() {}


void ClientView::filterChecked(int state){
  if(state == Qt::Unchecked){
    ui.filterDateSelection->setEnabled(false);
    carsModel->setBookedFilter(false);
  }
  else{
    ui.filterDateSelection->setEnabled(true);
    carsModel->setBookedFilter(true);
    carsModel->setBookedFrom(ui.filterFrom->date());
    carsModel->setBookedTo(ui.filterTo->date());
  }
}


void ClientView::filterFromChanged(const QDate & date){
  carsModel->setBookedFrom(date);
}

void ClientView::filterToChanged(const QDate & date){
  carsModel->setBookedTo(date);
}

void ClientView::bookClicked(){
  int selected_car_id;
  QItemSelectionModel * selection = ui.carsTable->selectionModel();
  if(!selection->hasSelection()){
    return;
  }
  else{
    QModelIndex idx = carsModel->index(selection->currentIndex().row(),0);
    selected_car_id = carsModel->data(idx).toInt();
  }

  QSqlRecord rec = bookingsModel->record();
  rec.setValue("klient_id", clientId);
  rec.setValue("samochod_id", selected_car_id);
  rec.setValue("data_od", ui.bookFrom->date().toString(DBConfig::dateFormat));
  rec.setValue("data_do", ui.bookTo->date().toString(DBConfig::dateFormat));

  bool ok = bookingsModel->insertRecord(-1, rec);

  if(!ok){
    qDebug() << bookingsModel->lastError().text();
    bookingsModel->select();
    QMessageBox::critical(this, "", QString::fromUtf8("Nie udało się dokonać rezerwacji"));
  }
  else{
    carsModel->select();
    bookingsModel->select();
    QMessageBox::information(this, "", QString::fromUtf8("Udało się dokonać rezerwacji"));
  }
}


