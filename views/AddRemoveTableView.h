/*
 * AddRemoveTableView.h
 *
 *  Created on: 13 Apr 2012
 *      Author: krzychu
 */

#ifndef ADDREMOVETABLEVIEW_H_
#define ADDREMOVETABLEVIEW_H_

#include <QWidget>
#include <QDialog>
#include <SqlModel.h>
#include "ui_add_remove_table_view.h"


class AddRemoveTableView : public QWidget{
    Q_OBJECT
  public:
    AddRemoveTableView(SqlModel * model, QDialog * addRecordDialog = 0,
        QWidget * parent = 0);

    virtual ~AddRemoveTableView();

  public slots:
    void removeItems();
    void addItem();

  private:
    Ui::AddRemoveTableView ui;
    SqlModel * model;
    QDialog * addRecordDialog;

};

#endif /* ADDREMOVETABLEVIEW_H_ */
