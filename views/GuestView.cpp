/*
 * GuestView.cpp
 *
 *  Created on: 11 Apr 2012
 *      Author: krzychu
 */

#include "GuestView.h"
#include <DBConfig.h>
#include <QSqlDatabase>

GuestView::GuestView(QWidget * parent)
  : QWidget(parent)
{
  ui.setupUi(this);
  model = new CarsModel(this, QSqlDatabase::database(DBConfig::connectionName));
  ui.carsView->setModel(model);
  model->select();
}

GuestView::~GuestView() {}

