/*
 * AdminView.h
 *
 *  Created on: 12 Apr 2012
 *      Author: krzychu
 */

#ifndef ADMINVIEW_H_
#define ADMINVIEW_H_

#include <QWidget>
#include "ui_admin.h"
#include <AddRemoveTableView.h>

#include <AddCarDialog.h>
#include <CarsModel.h>

#include <AddClientDialog.h>
#include <ClientsModel.h>

class AdminView : public QWidget{
  Q_OBJECT
  public:
    AdminView(QWidget * parent = 0);
    virtual ~AdminView();

  private:
    Ui::AdminWidget ui;

    CarsModel * carsModel;
    AddCarDialog * addCarDialog;
    AddRemoveTableView * carsView;

    ClientsModel * clientsModel;
    AddClientDialog * addClientDialog;
    AddRemoveTableView * clientsView;
};

#endif /* ADMINVIEW_H_ */
