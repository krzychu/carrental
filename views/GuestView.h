/*
 * GuestView.h
 *
 *  Created on: 11 Apr 2012
 *      Author: krzychu
 */

#ifndef GUESTVIEW_H_
#define GUESTVIEW_H_

#include <QWidget>
#include <CarsModel.h>
#include "ui_guest.h"

class GuestView : public QWidget{
  Q_OBJECT
  public:
    GuestView(QWidget * parent = 0);
    virtual ~GuestView();

  private:
    Ui::Guest ui;
    CarsModel * model;
};

#endif /* GUESTVIEW_H_ */
