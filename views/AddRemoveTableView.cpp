/*
 * AddRemoveTableView.cpp
 *
 *  Created on: 13 Apr 2012
 *      Author: krzychu
 */

#include "AddRemoveTableView.h"
#include <QDebug>

AddRemoveTableView::AddRemoveTableView(SqlModel * model_, QDialog * addRecord_,
    QWidget * parent)
  : QWidget(parent), model(model_), addRecordDialog(addRecord_)
{
  ui.setupUi(this);

  connect(ui.addButton, SIGNAL(clicked()), this, SLOT(addItem()));
  connect(ui.removeButton, SIGNAL(clicked()), this, SLOT(removeItems()));

  if(addRecordDialog == 0){
    ui.addButton->setEnabled(false);
  }

  ui.tableView->setModel(model);
  model->select();
}

AddRemoveTableView::~AddRemoveTableView() {}


void AddRemoveTableView::addItem(){
  addRecordDialog->show();
}


void AddRemoveTableView::removeItems(){
  // get selection
  QItemSelectionModel * selection = ui.tableView->selectionModel();
  if(!selection->hasSelection()){
    return;
  }

  // ask for confirmation

  // delete
  bool ok = model->removeRecords(selection->selectedRows());
}

