/*
 * AdminView.cpp
 *
 *  Created on: 12 Apr 2012
 *      Author: krzychu
 */

#include "AdminView.h"
#include <QSqlDatabase>
#include <DBConfig.h>
#include <AddRemoveTableView.h>

AdminView::AdminView(QWidget * parent)
  : QWidget(parent)
{
  ui.setupUi(this);


  carsModel = new CarsModel(this, QSqlDatabase::database(DBConfig::connectionName));
  addCarDialog = new AddCarDialog(carsModel, this);
  carsView = new AddRemoveTableView(carsModel, addCarDialog);
  QGridLayout * carsLayout = new QGridLayout;
  delete ui.carsTab->layout();
  ui.carsTab->setLayout(carsLayout);
  carsLayout->addWidget(carsView);


  clientsModel = new ClientsModel(this, QSqlDatabase::database(DBConfig::connectionName));
  addClientDialog = new AddClientDialog(clientsModel, this);
  QGridLayout * clientsLayout = new QGridLayout;
  clientsView = new AddRemoveTableView(clientsModel, addClientDialog);
  delete ui.clientsTab->layout();
  ui.clientsTab->setLayout(clientsLayout);
  clientsLayout->addWidget(clientsView);
}

AdminView::~AdminView() {}

