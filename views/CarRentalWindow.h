/*
 * CarRentalWindow.h
 *
 *  Created on: 11 Apr 2012
 *      Author: krzychu
 */

#ifndef CARRENTALWINDOW_H_
#define CARRENTALWINDOW_H_

#include <QMainWindow>
#include "ui_main.h"
#include <LoginDialog.h>

class CarRentalWindow : public QMainWindow{
  Q_OBJECT
  public:
    CarRentalWindow(QWidget * parent = 0);
    virtual ~CarRentalWindow();

    bool connectAs(int role);
    void disconnect();

  public slots:
    void login(int role, int id);
    void logout();

    void dbError(QString  what);

  private:
    Ui::MainWindow ui;
    LoginDialog * loginDialog;

    QGridLayout * contentLayout;
    QWidget * currentView;

    int role;
};

#endif /* CARRENTALWINDOW_H_ */
