/*
 * CarRentalWindow.cpp
 *
 *  Created on: 11 Apr 2012
 *      Author: krzychu
 */

#include "CarRentalWindow.h"
#include <QTimer>

#include <GuestView.h>
#include <AdminView.h>
#include <ClientView.h>

#include <QMessageBox>
#include <DBConfig.h>
#include <QtSql>

CarRentalWindow::CarRentalWindow(QWidget * parent )
  : QMainWindow(parent)
{
  loginDialog = new LoginDialog(this);
  ui.setupUi(this);
  currentView = 0;

  // set content layout
  contentLayout = new QGridLayout;
  ui.content->setLayout(contentLayout);

  connect(ui.logoutButton, SIGNAL(clicked()), this, SLOT(logout()));
  connect(loginDialog, SIGNAL(login(int,int)), this, SLOT(login(int, int)));
  //QTimer::singleShot(10, this, SLOT(logout()));
  disconnect();

  login(DBConfig::role::Client, 1);
}

CarRentalWindow::~CarRentalWindow() {

}

void CarRentalWindow::login(int role_, int id){
  loginDialog->hide();
  if(role_ == DBConfig::role::Client){
    // TODO: check if user exists and find his surname
  }
  role = role_;

  // connect to database as specified user
  if(!connectAs(role)){
    QMessageBox::critical(this,QString::fromUtf8("Błąd połączenia z bazą danych"),
     QString::fromUtf8("Nie udało się nawiązać połączenia z bazą danych"));
    logout();
    return;
  }

  // create view
  switch(role){
    case DBConfig::role::Guest :
      currentView = new GuestView;
    break;

    case DBConfig::role::Admin :
      currentView = new AdminView;
    break;

    case DBConfig::role::Client :
      currentView = new ClientView(id);
    break;
  }

  // display view
  ui.nameLabel->setText(QString::fromUtf8("Zalogowano jako: ")+
      DBConfig::roleNames[role_]);
  contentLayout->addWidget(currentView);
  ui.logoutButton->setEnabled(true);
}


void CarRentalWindow::logout(){

  if(currentView){
    contentLayout->removeWidget(currentView);
    delete currentView;
    disconnect();
  }

  ui.logoutButton->setEnabled(false);
  ui.nameLabel->setText(QString::fromUtf8("Nikt nie jest zalogowany"));
  loginDialog->show();
}



void CarRentalWindow::dbError(QString what){
  QMessageBox::critical(this,QString::fromUtf8("Błąd połączenia z bazą danych"), what);
}



bool CarRentalWindow::connectAs(int role){
  QSqlDatabase connection = QSqlDatabase::database(DBConfig::connectionName);

  if(!connection.isValid()){
    connection.setHostName(DBConfig::host);
    connection = QSqlDatabase::addDatabase("QPSQL",DBConfig::connectionName);
    connection.setDatabaseName(DBConfig::database);
  }
  else if(connection.isOpen()){
    connection.close();
  }

  if(connection.open(DBConfig::users[role], DBConfig::passwords[role])){
    ui.dbStatusLabel->setText(QString::fromUtf8("<font color='green'>Połączony</font>"));
    return true;
  }
  else{
    ui.dbStatusLabel->setText(QString::fromUtf8("<font color='red'>Rozłączony</font>"));
    qDebug() << connection.lastError().text();
    return false;
  }
}



void CarRentalWindow::disconnect(){
  ui.dbStatusLabel->setText(QString::fromUtf8("<font color='red'>Rozłączony</font>"));
  QSqlDatabase connection = QSqlDatabase::database(DBConfig::connectionName);
  if(connection.isOpen()){
    connection.close();
  }
}
