#include <iostream>
#include <QApplication>
#include <LoginDialog.h>
#include <QtGui>

int main(int argc, char ** argv){
  QApplication app(argc, argv);
  app.setStyle(new QPlastiqueStyle);
  LoginDialog ld;
  ld.show();

  return app.exec();
}
