#include <QtGui>
#include <GuestView.h>

int main(int argc, char ** argv){
  QApplication app(argc, argv);
  app.setStyle(new QPlastiqueStyle);
  GuestView gw;
  gw.show();


  return app.exec();
}
